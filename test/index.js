const chai = require("chai");
const request = require("supertest");
const app = require("../app");

const expect = chai.expect;

describe("App", () => {
    it("should return a 200 status code", done => {
        request(app)
            .get("/")
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    });
});